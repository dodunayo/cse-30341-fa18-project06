/* disk.c: SimpleFS disk emulator */

#include "sfs/disk.h"
#include "sfs/logging.h"

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

/* Internal Prototyes */

bool    disk_sanity_check(Disk *disk, size_t blocknum, const char *data);

/* External Functions */

/**
 *
 * Opens disk at specified path with the specified number of blocks by doing
 * the following:
 *
 *  1. Allocates Disk structure and sets appropriate attributes.
 *
 *  2. Opens file descriptor to specified path.
 *
 *  3. Truncates file to desired file size (blocks * BLOCK_SIZE).
 *
 * @param       path        Path to disk image to create.
 * @param       blocks      Number of blocks to allocate for disk image.
 *
 * @return      Pointer to newly allocated and configured Disk structure (NULL
 *              on failure).
 **/
Disk *	disk_open(const char *path, size_t blocks) {
    if(!path){
        fprintf(stderr, "Error. Bad path: %s\n", strerror(errno));
        return NULL;
    }

    Disk *nextDisk = calloc(1, sizeof(Disk));
    

    if(nextDisk == NULL){
        fprintf(stderr, "Error creating disk: %s\n", strerror(errno));
        return NULL;
    }
    
    nextDisk->fd = open(path, O_RDWR|O_CREAT, 0666);
    nextDisk->blocks = blocks;
    nextDisk->reads = 0;
    nextDisk->writes = 0;
    nextDisk->mounted = false;

    
    if(nextDisk->fd < 0){
        fprintf(stderr, "Error opening file: %s\n", strerror(errno));
        free(nextDisk);
        return NULL;
    }
 
    if(ftruncate(nextDisk->fd, (blocks * BLOCK_SIZE)) < 0){
        fprintf(stderr, "Error. ftruncate failed: %s\n", strerror(errno));
        free(nextDisk);
        return NULL;
    }
        
    return nextDisk;
}

/**
 * Close disk structure by doing the following:
 *
 *  1. Close disk file descriptor.
 *
 *  2. Report number of disk reads and writes.
 *
 *  3. Releasing disk structure memory.
 *
 * @param       disk        Pointer to Disk structure.
 */
void	disk_close(Disk *disk) {
    if((disk) && (disk->fd > 0)){
        printf("%lu disk block reads\n", disk->reads);
        printf("%lu disk block writes\n", disk->writes);
        close(disk->fd);
    }
    if(disk){
        free(disk);
    }
}

/**
 * Read data from disk at specified block into data buffer by doing the
 * following:
 *
 *  1. Performing sanity check.
 *
 *  2. Seeking to specified block.
 *
 *  3. Reading from block to data buffer (must be BLOCK_SIZE).
 *
 * @param       disk        Pointer to Disk structure.
 * @param       block       Block number to perform operation on.
 * @param       data        Data buffer.
 *
 * @return      Number of bytes read.
 *              (BLOCK_SIZE on success, DISK_FAILURE on failure).
 **/
ssize_t disk_read(Disk *disk, size_t block, char *data) {
    
    if(!disk_sanity_check(disk, block, data)){
        fprintf(stderr, "Error. Failed sanity check on read: %s\n", strerror(errno));
        return DISK_FAILURE;
    }
    
    if(lseek(disk->fd, (block * BLOCK_SIZE), SEEK_SET) < 0){
        fprintf(stderr, "Error. Lseek failed on read: %s\n", strerror(errno));
        return DISK_FAILURE;
    }
    
    if(read(disk->fd, data, BLOCK_SIZE) != BLOCK_SIZE){
        fprintf(stderr, "Error. Failed read (sys call) on read (disk): %s\n", strerror(errno));
        return DISK_FAILURE;
    }
        
    disk->reads++;
    return BLOCK_SIZE;
}

/**
 * Write data to disk at specified block from data buffer by doing the
 * following:
 *
 *  1. Performing sanity check.
 *
 *  2. Seeking to specified block.
 *
 *  3. Writing data buffer (must be BLOCK_SIZE) to disk block.
 *
 * @param       disk        Pointer to Disk structure.
 * @param       block       Block number to perform operation on.
 * @param       data        Data buffer.
 *
 * @return      Number of bytes written.
 *              (BLOCK_SIZE on success, DISK_FAILURE on failure).
 **/
ssize_t disk_write(Disk *disk, size_t block, char *data) {

    if(!disk_sanity_check(disk, block, data)){
        fprintf(stderr, "Error. Failed sanity check on write: %s\n", strerror(errno));
        return DISK_FAILURE;
    }
    
    if(lseek(disk->fd, (block * BLOCK_SIZE), SEEK_SET) < 0){
        fprintf(stderr, "Error. Lseek failed on write: %s\n", strerror(errno));
        return DISK_FAILURE;
    }
    
    if(write(disk->fd, data, BLOCK_SIZE) != BLOCK_SIZE){
        fprintf(stderr, "Error. Failed write (sys call) on write (disk): %s\n", strerror(errno));
        return DISK_FAILURE;
    }
    
    disk->writes++;
    return BLOCK_SIZE;
}

/* Internal Functions */

/**
 * Perform sanity check before read or write operation:
 *
 *  1. Check for valid disk.
 *
 *  2. Check for valid block.
 *
 *  3. Check for valid data.
 *
 * @param       disk        Pointer to Disk structure.
 * @param       block       Block number to perform operation on.
 * @param       data        Data buffer.
 *
 * @return      Whether or not it is safe to perform a read/write operation
 *              (true for safe, false for unsafe).
 **/
bool    disk_sanity_check(Disk *disk, size_t block, const char *data) {
    if(!disk){
        fprintf(stderr, "Error. Disk returns null: %s\n", strerror(errno));
        return false;
    }


    if(!data){
        fprintf(stderr, "Error. Data doesn't exist: %s\n", strerror(errno));
        return false;
    }

    if((block < 0) || (disk->blocks <= block)){
        fprintf(stderr, "Error. Block is invalid: %s\n", strerror(errno));
        return false;
    }       

    return true;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
