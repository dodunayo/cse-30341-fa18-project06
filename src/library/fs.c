/* fs.c: SimpleFS file system */

#include "sfs/fs.h"
#include "sfs/logging.h"
#include "sfs/utils.h"

#include <stdio.h>
#include <string.h>

ssize_t get_block_from_index(FileSystem *fs, Inode * inode, size_t block_number){
    if (block_number < POINTERS_PER_INODE){
    	return inode->direct[block_number];
    }
    else{
        if(!inode->indirect){
            return 0;
        }
        Block inDirect;
        disk_read(fs->disk, inode->indirect, inDirect.data);
        return inDirect.pointers[block_number - POINTERS_PER_INODE];
    }
}

bool initialize_block_bitmap(FileSystem *fs) {
    Block block;
    fs->free_blocks = calloc(fs->meta_data.blocks,sizeof(bool));
    for (uint32_t z = fs->meta_data.inode_blocks + 1; z < fs->meta_data.blocks; z++) {
        fs->free_blocks[z] = true;
    }
    for(uint32_t i = 1; i <= fs->meta_data.inode_blocks; i++){
        if(disk_read(fs->disk, i, block.data) == DISK_FAILURE) {
            return false;
        }
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++){
            if (block.inodes[j].valid) {
                for(uint32_t k = 0; k < POINTERS_PER_INODE; k++){
                    if (block.inodes[j].direct[k]) {
                        fs->free_blocks[block.inodes[j].direct[k]] = false;
                    }
                }
                if (block.inodes[j].indirect) {
                    fs->free_blocks[block.inodes[j].indirect] = false;
                    Block Indirect;
                    if (disk_read(fs->disk, block.inodes[j].indirect, Indirect.data) == DISK_FAILURE) {
                        return false;
                    }
                    for (uint32_t h = 0; h < POINTERS_PER_BLOCK; h++) {
                        if(Indirect.pointers[h]) {
                            fs->free_blocks[Indirect.pointers[h]] = false;
                        }
                    }
                }
            }
        }
    }
    return true;
}




/**
 * Debug FileSystem by doing the following
 *
 *  1. Read SuperBlock and report its information.
 *
 *  2. Read Inode Table and report information about each Inode.
 *
 * @param	   disk		Pointer to Disk structure.
 **/
void	fs_debug(Disk *disk) {
        Block block;

	/* Read SuperBlock */
	if (disk_read(disk, 0, block.data) == DISK_FAILURE) {
		return;
	}

	printf("SuperBlock:\n");
	
	if (block.super.magic_number == MAGIC_NUMBER){
	    printf("    magic number is valid\n");
	}
        else{
	    printf("    magic number is not valid\n");

	}
	printf("    %u blocks\n"		 , block.super.blocks);
	printf("    %u inode blocks\n"   , block.super.inode_blocks);
	printf("    %u inodes\n"		 , block.super.inodes);

	/* Read Inodes */
	for (uint32_t i = 1; i <= block.super.inode_blocks; i++)
	{
		Block iBlock;
		if (disk_read(disk, i, iBlock.data) == DISK_FAILURE)
		{
			return;
		}
		for (uint32_t j = 0; j < INODES_PER_BLOCK; j++)
		{
			if(iBlock.inodes[j].valid)
			{
				printf("Inode %d:\n", j);
				printf("    size: %u bytes\n"	, iBlock.inodes[j].size);
				printf("    direct blocks:");
				for(uint32_t k = 0; k < POINTERS_PER_INODE; k++)
				{
					if(iBlock.inodes[j].direct[k])
					{
						printf(" %u", iBlock.inodes[j].direct[k]);
					}
				}
				printf("\n");
				if (iBlock.inodes[j].indirect)
				{
					printf("    indirect block: %u\n", iBlock.inodes[j].indirect);
					printf("    indirect data blocks:");
					Block inDirect;
					disk_read(disk, iBlock.inodes[j].indirect, inDirect.data);
					for(uint32_t l = 0; l < POINTERS_PER_BLOCK; l++)
					{
						if (inDirect.pointers[l])
						{
							printf(" %u", inDirect.pointers[l]);
						}
					}
					printf("\n");
				}
			}
		}
	}
}

/**
 * Format Disk by doing the following:
 *
 *  1. Write SuperBlock (with appropriate magic number, number of blocks,
 *  number of inode blocks, and number of inodes).
 *
 *  2. Clear all remaining blocks.
 *
 * Note: Do not format a mounted Disk!
 *
 * @param	   disk		Pointer to Disk structure.
 * @return	  Whether or not all disk operations were successful.
 **/
bool	fs_format(Disk *disk) {

    if (disk->mounted == true){
    	return false;
    }

    Block block;
    for (int i = 0; i < BLOCK_SIZE; i++){
    	block.data[i] = 0;
    }
    block.super.blocks = disk->blocks;
    block.super.magic_number = MAGIC_NUMBER;

    size_t value;
    if ((block.super.blocks % 10) == 0){
    	value = 0;
    }
    else{
    	value = 1;
    }
    block.super.inode_blocks = ((block.super.blocks / 10) + value);
    block.super.inodes = block.super.inode_blocks * INODES_PER_BLOCK;
    disk_write(disk, 0, block.data);

    Block iBlock;
    for (uint32_t j = 0; j < INODES_PER_BLOCK; j++)
    {
    	iBlock.inodes[j].size = 0;
    	iBlock.inodes[j].valid = 0;
    	for (uint32_t k = 0; k < POINTERS_PER_INODE; k++){
    		iBlock.inodes[j].direct[k] = 0;
    	}
    	iBlock.inodes[j].indirect = 0;
    }

    for(uint32_t i = 1; i <= block.super.inode_blocks; i++){
    	disk_write(disk, i, iBlock.data);
    }

    Block dBlock;
    for(uint32_t i = 0; i < BLOCK_SIZE; i++){
    	dBlock.data[i] = 0;
    }
    for(uint32_t i = block.super.inode_blocks + 1; i < block.super.blocks; i++){
    	disk_write(disk, i, dBlock.data);
    }

    return true;
}

/**
 * Mount specified FileSystem to given Disk by doing the following:
 *
 *  1. Read and check SuperBlock (verify attributes).
 *
 *  2. Record FileSystem disk attribute and set Disk mount status.
 *
 *  3. Copy SuperBlock to FileSystem meta data attribute
 *
 *  4. Initialize FileSystem free blocks bitmap.
 *
 * Note: Do not mount a Disk that has already been mounted!
 *
 * @param	   fs	  Pointer to FileSystem structure.
 * @param	   disk	Pointer to Disk structure.
 * @return	  Whether or not the mount operation was successful.
 **/
bool	fs_mount(FileSystem *fs, Disk *disk) {
    if(!fs || !disk){
        return false;
    }

    if (disk->mounted == true){
        return false;
    }
    Block block;
    
    if (disk_read(disk, 0, block.data) == DISK_FAILURE)
    {
        return false;
    }
    
    if(block.super.magic_number != MAGIC_NUMBER ){
        return false;
    }

    if(block.super.blocks != disk->blocks)
    {
        return false;
    }

    size_t value = 0;
    if(block.super.blocks % 10 == 0){
        value = 0;
    }
    else{
        value = 1;
    }
    if(block.super.inode_blocks != ((block.super.blocks / 10) + value)){
        return false;
    }
    if(block.super.inodes != block.super.inode_blocks * INODES_PER_BLOCK){
        return false;
    }
    disk->mounted = true;
    fs->disk = disk;
    memcpy(&fs->meta_data, &block.super, sizeof(SuperBlock));

    if(!initialize_block_bitmap(fs)){
        return false;
    }
    return true;
}

/**
 * Unmount FileSystem from internal Disk by doing the following:
 *
 *  1. Set Disk mounted status and FileSystem disk attribute.
 *
 *  2. Release free blocks bitmap.
 *
 * @param	   fs	  Pointer to FileSystem structure.
 **/
void	fs_unmount(FileSystem *fs) {
    if(!fs || !fs->disk){
    	return;
    }
    fs->disk->mounted = false;
    fs->disk = NULL;
    free(fs->free_blocks);
    fs->free_blocks = NULL;
}

/**
 * Allocate an Inode in the FileSystem Inode table by doing the following:
 *
 *  1. Search Inode table for free inode.
 *
 *  2. Reserve free inode in Inode table.
 *
 * Note: Be sure to record updates to Inode table to Disk.
 *
 * @param	   fs	  Pointer to FileSystem structure.
 * @return	  Inode number of allocated Inode.
 **/
ssize_t fs_create(FileSystem *fs) {
    Block block;
    for(uint32_t i = 1; i < (fs->meta_data.inode_blocks + 1); i++){
        if(disk_read(fs->disk, i, block.data) == DISK_FAILURE){
            return false;
        }
	for(uint32_t j = 0; j < INODES_PER_BLOCK; j++){
	    if(!block.inodes[j].valid){
	    	block.inodes[j].valid = 1;
	    	for(uint32_t k = 0; k < POINTERS_PER_INODE; k++){
	    	    block.inodes[j].direct[k] = 0;
	    	}
	    	block.inodes[j].indirect = 0;
	    	disk_write(fs->disk, i, block.data);
	    	return(((i - 1) * INODES_PER_BLOCK) + j);
	    }
	}
    }
    return -1;
}

/**
 * Remove Inode and associated data from FileSystem by doing the following:
 *
 *  1. Load and check status of Inode.
 *
 *  2. Release any direct blocks.
 *
 *  3. Release any indirect blocks.
 *
 *  4. Mark Inode as free in Inode table.
 *
 * @param	   fs			  Pointer to FileSystem structure.
 * @param	   inode_number	Inode to remove.
 * @return	  Whether or not removing the specified Inode was successful.
 **/
bool	fs_remove(FileSystem *fs, size_t inode_number) {
    if(!fs){
        return false;
    }
    Block iBlock;

    disk_read(fs->disk, inode_number / INODES_PER_BLOCK + 1, iBlock.data);
    size_t iOffset = inode_number % INODES_PER_BLOCK;
    if (iBlock.inodes[iOffset].valid == false)
    {
        return false;
    }
    iBlock.inodes[iOffset].valid = false;
    iBlock.inodes[iOffset].size = 0;
    for (int k = 0; k < POINTERS_PER_INODE; k++)
    {
        fs->free_blocks[iBlock.inodes[iOffset].direct[k]] = true;
        iBlock.inodes[iOffset].direct[k] = 0;
    }
    if (iBlock.inodes[iOffset].indirect)
    {
        Block idBlock;
        disk_read(fs->disk, iBlock.inodes[iOffset].indirect, idBlock.data);
        for (int i = 0; i < POINTERS_PER_BLOCK; i++)
        {
            if (idBlock.pointers[i] != 0)
            {
                fs->free_blocks[idBlock.pointers[i]] = true;
            }
            fs->free_blocks[iBlock.inodes[iOffset].indirect] = true;
        }
    }
    iBlock.inodes[iOffset].indirect = 0;
    disk_write(fs->disk, inode_number / INODES_PER_BLOCK + 1, iBlock.data);
    return true;
}

/**
 * Return size of specified Inode.
 *
 * @param	   fs			  Pointer to FileSystem structure.
 * @param	   inode_number	Inode to remove.
 * @return	  Size of specified Inode (-1 if does not exist).
 **/
ssize_t fs_stat(FileSystem *fs, size_t inode_number) {
	Block block;
	for (uint32_t i = 1; i <= fs->meta_data.inode_blocks; i++) {
		disk_read(fs->disk, i, block.data);
		for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
			Inode inode = block.inodes[j];
			if(inode_number == ((i - 1) * INODES_PER_BLOCK) + j) {
				if(block.inodes[j].valid) {
					return inode.size;
				} else {
					return -1;
				}
			}
		}
	}
	return -1;
}

ssize_t allocate_free_block(FileSystem *fs) {
    for(uint32_t z = fs->meta_data.inode_blocks + 1; z < fs->meta_data.blocks; z++){
    	if (fs->free_blocks[z]) {
    		fs->free_blocks[z] = false;
    		return z;
    	}
    }
    return -1;
}

ssize_t inode_allocate_free_block(FileSystem *fs, Inode *inode) {
	for (int i = 0; i < POINTERS_PER_INODE; i++) {
		if (!inode->direct[i]) {
			size_t block_pointer = allocate_free_block(fs);
			if(block_pointer == -1)
				return -1;
			inode->direct[i] = block_pointer;
			return block_pointer;
		}
	}
	Block pointers;
	if (!inode->indirect) {
	    inode->indirect = allocate_free_block(fs);
	    if (inode->indirect == -1){
		return -1;
            }
	    for (int i = 1; i < POINTERS_PER_BLOCK; i++){ 
		pointers.pointers[i] = 0;
            }
	    pointers.pointers[0] = allocate_free_block(fs);
	    if (pointers.pointers[0] == -1){
	        return -1;
            }
	    disk_write(fs->disk, inode->indirect, pointers.data);
	    return pointers.pointers[0];
	}
	disk_read(fs->disk, inode->indirect, pointers.data);
	for (int i = 1; i < POINTERS_PER_BLOCK; i++) {
	    if (pointers.pointers[i] == 0 ) {
	    	pointers.pointers[i] = allocate_free_block(fs);
	    	if(pointers.pointers[i] == -1){
        	    return -1;
                }
		disk_write(fs->disk, inode->indirect, pointers.data);
		return pointers.pointers[i];
	    }
        }
        return -1;
}

/**
 * Read from the specified Inode into the data buffer exactly length bytes
 * beginning from the specified offset by doing the following:
 *
 *  1. Load Inode information.
 *
 *  2. Continuously read blocks and copy data to buffer.
 *
 *  Note: Data is read from direct blocks first, and then from indirect blocks.
 *
 * @param	   fs			  Pointer to FileSystem structure.
 * @param	   inode_number	Inode to read data from.
 * @param	   data			Buffer to copy data to.
 * @param	   length		  Number of bytes to read.
 * @param	   offset		  Byte offset from which to begin reading.
 * @return	  Number of bytes read (-1 on error).
 **/
ssize_t fs_read(FileSystem *fs, size_t inode_number, char *data, size_t length, size_t offset) {
	Block iBlock;
	disk_read(fs->disk, (inode_number / INODES_PER_BLOCK) + 1, iBlock.data);
	Inode *inode = &iBlock.inodes[inode_number % INODES_PER_BLOCK];
	if(!inode->valid){
		return -1;
	}
	if(offset > inode->size){
		return -1;
	}
	
	size_t block_index = offset / BLOCK_SIZE;
	if(block_index > (inode->size / BLOCK_SIZE) + (length % BLOCK_SIZE == 0 ? 0 : 1)){
		return -1;
	}
	
	uint32_t writeCount = 0;
	uint32_t index = offset;
	
	Block dataBlock;
	
	while((writeCount < length) && index < inode->size){
	    size_t block_pointer = get_block_from_index(fs, inode, block_index);
	    disk_read(fs->disk, block_pointer, dataBlock.data);
	    for(int i = (offset % BLOCK_SIZE); (i < BLOCK_SIZE) && (writeCount < length) && (index < inode->size); i++){
	    	data[writeCount] = dataBlock.data[i];
	    	writeCount++;
	    	index++;
	    }
	    offset = 0;
	    block_index++;
	}
	
	return writeCount;
}

/**
 * Write to the specified Inode from the data buffer exactly length bytes
 * beginning from the specified offset by doing the following:
 *
 *  1. Load Inode information.
 *
 *  2. Continuously copy data from buffer to blocks.
 *
 *  Note: Data is read from direct blocks first, and then from indirect blocks.
 *
 * @param	   fs			  Pointer to FileSystem structure.
 * @param	   inode_number	Inode to write data to.
 * @param	   data			Buffer with data to copy
 * @param	   length		  Number of bytes to write.
 * @param	   offset		  Byte offset from which to begin writing.
 * @return	  Number of bytes read (-1 on error).
 **/
ssize_t fs_write(FileSystem *fs, size_t inode_number, char *data, size_t length, size_t offset) {
	size_t block_offset = offset;
	Block inode_block;
	disk_read(fs->disk, inode_number / INODES_PER_BLOCK + 1, inode_block.data);
	Inode * inode = &inode_block.inodes[inode_number % INODES_PER_BLOCK];
	if (!inode->valid){
		return -1;
        }
	if (offset > inode->size){
		return -1;
        }

	uint32_t block_index = offset / BLOCK_SIZE;
	uint32_t writeCount = 0;

	Block data_block;
	while (writeCount < length) {
	    size_t block_pointer = get_block_from_index(fs, inode, block_index);
	    if(block_pointer == 0) {
	    	block_pointer = inode_allocate_free_block(fs, inode);
	    	if (block_pointer == -1) {
	    		if(offset + writeCount > inode->size){
	    		    inode->size = offset + writeCount;
                        }
	    		disk_write(fs->disk, inode_number / INODES_PER_BLOCK + 1, inode_block.data);
	    		return writeCount;
	    	}
	    }
		
	    disk_read(fs->disk, block_pointer, data_block.data);
	    for (int i = block_offset % BLOCK_SIZE; i < BLOCK_SIZE && writeCount < length; i++) {
	    	data_block.data[i] = data[writeCount];
	    	writeCount++;
	    }
	    block_offset = 0;
	    disk_write(fs->disk, block_pointer, data_block.data);

	    block_index++;
	}
	    if (offset + writeCount > inode->size){
	        inode->size = offset + writeCount;
            }
	    disk_write(fs->disk, inode_number / INODES_PER_BLOCK + 1, inode_block.data);
	return writeCount;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
