# Project 06: Simple File System

This is [Project 06] of [CSE.30341.FA18].

## Members

1. Thomas Plummer (tplumme2@nd.edu)
2. Seun Odun-Ayo (dodunayo@nd.edu)
3. Angel Rodriguez (arodri27@nd.edu)

## Errata

The read and write numbers are slightly off from the tests.

## Extra Credit

> Describe what extra credit (if any) that you implemented.

[Project 06]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/project06.html
[CSE.30341.FA18]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/
